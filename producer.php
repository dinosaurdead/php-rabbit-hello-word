<?php
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

// Create a connection to the server
$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

// Create a channel
$channel->queue_declare('hello', false, false, false, false);


// Create a message
$msg = new AMQPMessage('Hello World!');

// Publish the messa in channel
$channel->basic_publish($msg, '', 'hello');


echo " [x] Sent 'Hello World!'\n";